import pygame
import json
import math
pygame.init()

WHITE =     (255, 255, 255)
BLAU =      (  0,   0, 255)
GRUEN =     (  0, 255,   0)
GELB =      (255, 255, 0)
ROT =       (255,   0,   0)
DIGITS = 5
KEYS = ["start_blau", "start_rot", "start_gelb", "start_grün"]
[KEYS.append(str(x + 1)) for x in range(99)]
#[KEYS.append(str(x + 1)) for x in range(3)]
KEYS.append("ziel")
POSITIONS_FILE = 'data/positions.json'
DIAMETER = 0.05

display_info = pygame.display.Info()
dw, dh = display_info.current_w - 1100, display_info.current_h - 200
display_surface = pygame.display.set_mode((dw, dh))

board_image = pygame.image.load("./images/board.jpg")
iw, ih = board_image.get_size() # image size
rw, rh = dw / iw, dh / ih # image / screen ratio
if rw < rh:
    gw, gh = dw, int(ih / iw * dw)
else:
    gw, gh = int(iw / ih * dh), dh

board_image = pygame.transform.scale(board_image, (gw, gh))

# get positions
try:
    f = open(POSITIONS_FILE, "r")
    POSITIONS = json.load(f)
    print(p)
except:
    pass


# functions
def inDiam(key, pos):
    dx = pos[0] - POSITIONS[key][0]
    dy = pos[1] - POSITIONS[key][1]
    r = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
    return (r * 2) < DIAMETER


def maxl(color, pos):
    x = pos[0]
    y = pos[1]
    pygame.draw.circle(display_surface, color, (int(x * gw), int(y * gh)), int(DIAMETER / 4 * ((gw + gh) / 2)))

def test(*a):
    print(a)

display_surface.fill(WHITE)
# copying the image to (0, 0) coordinate.
display_surface.blit(board_image, (0, 0))


# display data
#for val in POSITIONS.values():
#    print("item: {}".format(val))
#    pygame.draw.circle(display_surface, BLUE, (int(val[0] * gw), int(val[1] * gh)), 30)

pygame.display.update()

spielaktiv = True
spielmenue = True
gruen = False
gelb = False
rot = False
blau = False

i = 0
pos_dict = dict()

# Schleife Hauptprogramm
while spielaktiv:
    # Überprüfen, ob Nutzer eine Aktion durchgeführt hat
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            spielaktiv = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                spielaktiv = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()
            pos_rel = round(pos[0] / gw, DIGITS), round(pos[1] / gh, DIGITS)
            if spielmenue:
                if inDiam("start_grün", pos_rel):
                    gruen = not gruen
                if inDiam("start_gelb", pos_rel):
                    gelb = not gelb
                if inDiam("start_rot", pos_rel):
                    rot = not rot
                if inDiam("start_blau", pos_rel):
                    blau = not blau 
# get data
#        elif event.type == pygame.MOUSEBUTTONDOWN:
#            pos = pygame.mouse.get_pos()
#            pos_rel = round(pos[0] / gw, DIGITS), round(pos[1] / gh, DIGITS)
#            pos_dict[KEYS[i]] = pos_rel
#            i += 1
#            if i == len(KEYS):
#                spielaktiv = False

    # Spiellogik hier integrieren

    # Spielfeld/figur(en) zeichnen (davor Spielfeld löschen)

    # Fenster aktualisieren
    display_surface.fill(WHITE)
    # copying the image to (0, 0) coordinate.
    display_surface.blit(board_image, (0, 0))
    if spielmenue and gruen:
        maxl(GRUEN, POSITIONS["start_grün"])
    if spielmenue and gelb:
        maxl(GELB, POSITIONS["start_gelb"])
    if spielmenue and rot:
        maxl(ROT, POSITIONS["start_rot"])
    if spielmenue and blau:
        maxl(BLAU, POSITIONS["start_blau"])
    pygame.display.update()

    # Refresh-Zeiten festlegen

# save data
#with open(POSITIONS_FILE, 'w') as outfile:
#    #json.dump(pos_dict, outfile, indent=4, sort_keys=True))
#    json.dump(pos_dict, outfile, indent=4)
