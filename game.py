import pygame
import json
import math
import time
import random
import traceback
pygame.init()
pygame.mixer.init()

WHITE =     (255, 255, 255)
GRUEN =     (  0, 255,   0)
GELB =      (255, 255, 0)
ROT =       (255,   0,   0)
BLAU =      (  0,   0, 255)
DIGITS = 5
POSITIONS_FILE = 'data/positions.json'
LEITERN_FILE = 'data/leitern.json'
DIAMETER = 0.05
SPIELER_MAP = {
    0: "grün",
    1: "gelb",
    2: "rot",
    3: "blau"
}
COLOR_MAP = {
    0: GRUEN,
    1: GELB,
    2: ROT,
    3: BLAU
}

display_info = pygame.display.Info()
dw, dh = display_info.current_w - 1100, display_info.current_h - 200
display_surface = pygame.display.set_mode((dw, dh))

board_image = pygame.image.load("./images/board.jpg")
iw, ih = board_image.get_size() # image size
rw, rh = dw / iw, dh / ih # image / screen ratio
if rw < rh:
    gw, gh = dw, int(ih / iw * dw)
else:
    gw, gh = int(iw / ih * dh), dh

board_image = pygame.transform.scale(board_image, (gw, gh))
#pygame.mixer.music.load("sound/roll.wav")
fx_roll = pygame.mixer.Sound('sound/roll.wav')
fx_up = pygame.mixer.Sound('sound/up2.wav')
fx_down = pygame.mixer.Sound('sound/down2.wav')
fx_win = pygame.mixer.Sound('sound/win.wav')

font = pygame.font.SysFont('Arial', 25)

# get positions
try:
    f = open(POSITIONS_FILE, "r")
    POSITIONS = json.load(f)
except:
    print("fatal error on loading positions")
    exit(1)

# get leitern
try:
    f = open(LEITERN_FILE, "r")
    LEITERN = json.load(f)
    LEITERN = {int(k):int(v) for k,v in LEITERN.items()} # convert json file string keys to int
except:
    print("fatal error on loading leitern")
    print(traceback.format_exc())
    exit(1)


# functions
def inDiam(key, pos):
    dx = pos[0] - POSITIONS[key][0]
    dy = pos[1] - POSITIONS[key][1]
    r = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
    return (r * 2) < DIAMETER

def inRect(rect, pos):
    return pos[0] > rect[0] and pos[0] < (rect[0] + rect[2]) and pos[1] > rect[1] and pos[1] < (rect[1] + rect[3]) 

def percent_to_pix(*coords):
    return [int(i * (gw if i %2 else gh)) for i in coords]

def maxl(color, pos):
    x = pos[0]
    y = pos[1]
    pygame.draw.circle(display_surface, color, percent_to_pix(x, y), int(DIAMETER / 4 * ((gw + gh) / 2)))

def posi(spieler, num):
    if num == 0:
        return POSITIONS["start_" + SPIELER_MAP[spieler]]
    if num > 0:
        delta = 0.01
        if spieler == 0:
            mx = -delta
            my = -delta
        elif spieler == 1:
            mx = +delta
            my = -delta
        elif spieler == 2:
            mx = -delta
            my = +delta
        elif spieler == 3:
            mx = +delta
            my = +delta
        return (POSITIONS[str(num)][0] + mx, POSITIONS[str(num)][1] + my)

def next_player(num):
    for i in range(1, 5):
        ii = (num + i) % 4
        if pos_map[ii] >= 0 and pos_map[ii] < 100:
            if ii in aussetzen:
                aussetzen.remove(ii)
            else:
                return (ii)
    return -1

def window_ok(text):
    # Main frame
    pygame.draw.rect(display_surface, GELB, SUB)
    pygame.draw.rect(display_surface, ROT, SUB, 10)
    # Text
    display_surface.blit(font.render(text, True, ROT), percent_to_pix(0.12, 0.22))
    # OK button
    pygame.draw.rect(display_surface, BLAU, OKB)
    pygame.draw.rect(display_surface, ROT, OKB, 10)
    # Text
    display_surface.blit(font.render('OK', True, ROT), percent_to_pix(0.47, 0.31))
    #rect = pygame.Rect(25, 25, 100, 50)
    #sub = screen.subsurface(rect)

def window_dice(spieler_name, dice):
    # Main frame
    pygame.draw.rect(display_surface, GELB, SUB)
    pygame.draw.rect(display_surface, ROT, SUB, 10)
    # Text
    display_surface.blit(font.render('Spieler {} ist dran'.format(spieler_name), True, ROT), percent_to_pix(0.12, 0.22))
    # OK button
    pygame.draw.rect(display_surface, BLAU, DIC)
    pygame.draw.rect(display_surface, ROT, DIC, 10)
    # Text
    display_surface.blit(font.render('Würfeln', True, ROT), percent_to_pix(0.27, 0.31))
    if dice > 0:
        display_surface.blit(font.render(str(dice), True, ROT), percent_to_pix(0.47, 0.31))
    #rect = pygame.Rect(25, 25, 100, 50)
    #sub = screen.subsurface(rect)

def test(*a):
    print(a)

#GUI positions
SUB = percent_to_pix(0.1, 0.2, 0.8, 0.2)
OKB = percent_to_pix(0.45, 0.29, 0.1, 0.08)
DIC = percent_to_pix(0.25, 0.29, 0.15, 0.08)

display_surface.fill(WHITE)
# copying the image to (0, 0) coordinate.
display_surface.blit(board_image, (0, 0))

pygame.display.update()

spielstate = 0 # -1 .. exit, 0 .. init menue, 1 .. dice, 2 .. show result, 3 .. move, 4 .. finish
spieler = -1
aussetzen = list() # Spieler die auf Feld drei kamen und aussetzen müssen.
jump = -1 # leitern

i = 0
zzz = 0.1
pos_map = {
  0: -1,
  1: -1,
  2: -1,
  3: -1,
}
dice = 0
orientation = 1
txt = ""
txt2 = ""

# Schleife Hauptprogramm
while spielstate >= 0:
    if spielstate == 2:
        spielstate =3 
    # Überprüfen, ob Nutzer eine Aktion durchgeführt hat
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            spielstate = -1
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                spielstate = -1
        elif event.type == pygame.MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()
            pos_rel = round(pos[0] / gw, DIGITS), round(pos[1] / gh, DIGITS)
            if spielstate == 0:
                if inDiam("start_grün", pos_rel):
                    pos_map[0] = -(1 + pos_map[0])
                if inDiam("start_gelb", pos_rel):
                    pos_map[1] = -(1 + pos_map[1])
                if inDiam("start_rot", pos_rel):
                    pos_map[2] = -(1 + pos_map[2])
                if inDiam("start_blau", pos_rel):
                    pos_map[3] = -(1 + pos_map[3])
                if inRect(OKB, pos):
                    for i in reversed(range(4)):
                        if pos_map[i] == 0:
                            spieler = i
                            spielstate = 1
                    if spielstate == 0:
                        spielstate = -1 # kein Spieler ausgewählt --> exit
            elif spielstate == 1:
                if inRect(DIC, pos):
                    #pygame.mixer.music.play()
                    fx_roll.play()
                    dice = random.randint(1, 6)
                    orientation = 1
                    spielstate = 2
            elif spielstate == 4:
                if inRect(OKB, pos):
                    exit()

    # Spiellogik hier integrieren

    # Spielfeld/figur(en) zeichnen (davor Spielfeld löschen)

    # Fenster aktualisieren
    display_surface.fill(WHITE)
    # copying the image to (0, 0) coordinate.
    display_surface.blit(board_image, (0, 0))

    for i in range(4):
        if pos_map[i] >= 0:
            maxl(COLOR_MAP[i], posi(i, pos_map[i]))

    if spielstate == 0:
        window_ok('Mitspieler durch klicken auf die Start-Felder auswählen')
        zzz = 0.1
    elif spielstate in (1, 2) and pos_map[spieler] >= 0:
        window_dice(SPIELER_MAP[spieler], dice)
        zzz = 0.1 if dice == 0 else 0.7
    elif spielstate == 3:
        dice -= 1
        if pos_map[spieler] == 100:
            orientation = -1
        pos_map[spieler] += 1 * orientation
        if jump != -1:
            pos_map[spieler] = jump
            jump = -1
            spieler = next_player(spieler)
            if spieler == -1:
                spielstate = 4
            else:
                spielstate = 1
        elif dice == 0:
            if pos_map[spieler] == 100:
                fx_win.play()
            if pos_map[spieler] == 3:
                aussetzen.append(spieler)
            if pos_map[spieler] in LEITERN:
                jump = LEITERN[pos_map[spieler]]
                if pos_map[spieler] > jump:
                    fx_down.play()
                else:
                    fx_up.play()
            else:
                spieler = next_player(spieler)
                if spieler == -1:
                    spielstate = 4
                else:
                    spielstate = 1
        zzz = 0.3
    elif spielstate == 4:
        window_ok('Alle Spieler sind im Ziel - Bravo!')
        spielstate = 4
        zzz = 0.1

    pygame.display.update()
    txt = "state: {}, spieler: {}, pos_map {}".format(spielstate, spieler, pos_map)
    if (txt != txt2):
        print(txt)
    txt2 = txt

    # Refresh-Zeiten festlegen
    time.sleep(zzz)

